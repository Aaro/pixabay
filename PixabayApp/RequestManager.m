//
//  RequestManager.m
//  PixabayApp
//
//  Created by Надежда Шальнова on 24.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import "RequestManager.h"


@implementation RequestManager

NSString *const urlStr = @"https://pixabay.com/api/?key=5439837-c4bdd9975da100390f6e17e77&q=";
NSTimeInterval timeoutIntervalForRequest = 5.0;
NSTimeInterval timeoutIntervalForResource = 10.0;


+ (instancetype)sharedInstance {
    static RequestManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[RequestManager alloc] init];
    });
    return sharedInstance;
}


- (void)sendGetListRequestSuccess:(OnSuccess)success failure:(OnFailure)failure{
    NSMutableURLRequest *request = [self getRequest];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.timeoutIntervalForRequest = timeoutIntervalForRequest;
    sessionConfiguration.timeoutIntervalForResource = timeoutIntervalForResource;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
                                                if (error) {
                                                  failure(error);
                                                }
                                                if (response != nil) {
                                                    if (success != nil) {
                                                        success([ParseManager parseListRequest:data]);
                                                    }
                                                } else if (failure != nil) {
                                                    failure(error);
                                                }
                                            }];
    [task resume];
}

- (NSMutableURLRequest *)getRequest {
    NSURL * url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:url];
    return request;
}



- (void)sendGetListRequestWithText:(NSString *)text success:(OnSuccess)success failure:(OnFailure)failure {
    NSMutableURLRequest *request = [self getRequestWithText:text];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.timeoutIntervalForRequest = timeoutIntervalForRequest;
    sessionConfiguration.timeoutIntervalForResource = timeoutIntervalForResource;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
                                                if (error) {
                                                    failure(error);
                                                }
                                                if (response != nil) {
                                                    if (success != nil) {
                                                        success([ParseManager parseListRequest:data]);
                                                    }
                                                } else if (failure != nil) {
                                                    failure(error);
                                                }
                                            }];
    [task resume];
}

- (NSMutableURLRequest *)getRequestWithText:(NSString*) text {
    NSString *newStringURL =  [[urlStr stringByAppendingString:text] stringByAppendingString: @"&image_type=photo&pretty=true"];
    NSURL *url = [NSURL URLWithString:newStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:url];
    return request;
}


@end

