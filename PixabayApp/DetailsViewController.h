//
//  DetailsViewController.h
//  PixabayApp
//
//  Created by Надежда Шальнова on 24.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageInfo.h"

@interface DetailsViewController : UIViewController

- (void)setImageInfo:(ImageInfo*) info;

@end
