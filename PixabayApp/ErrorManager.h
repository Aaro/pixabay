//
//  ErrorManager.h
//  PixabayApp
//
//  Created by Надежда Шальнова on 30.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ErrorManager : NSObject

+ (void)showErrorMessange:(NSString *)errorText onVC:(UIViewController *)vc;

@end
