//
//  AppDelegate.h
//  PixabayApp
//
//  Created by Надежда Шальнова on 24.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

