//
//  ErrorManager.m
//  PixabayApp
//
//  Created by Надежда Шальнова on 30.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import "ErrorManager.h"

@implementation ErrorManager

+ (void)showErrorMessange:(NSString *)errorText onVC:(UIViewController *)vc {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Error!"
                                              message:errorText
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        
        UIAlertAction *cansel = [UIAlertAction actionWithTitle:@"cansel" style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action) {
                                                       }];
        [alertController addAction:cansel];
        [vc presentViewController:alertController animated:YES completion:nil];
}

@end
