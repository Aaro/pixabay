//
//  ImageInfo.h
//  PixabayApp
//
//  Created by Надежда Шальнова on 24.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageInfo : NSObject
    
@property (strong) NSNumber* previewHeight;
@property (strong) NSNumber* likes;
@property (strong) NSNumber* favorites;
@property (strong) NSArray*  tags;
@property (strong) NSNumber* webformatHeight;
@property (strong) NSNumber* views;
@property (strong) NSNumber* webformatWidth;
@property (strong) NSNumber* previewWidth;
@property (strong) NSNumber* comments;
@property (strong) NSNumber* downloads;
@property (strong) NSString* pageURL;
@property (strong) NSString* previewURL;
@property (strong) NSString* webformatURL;
@property (strong) NSNumber* imageWidth;
@property (strong) NSNumber* user_id;
@property (strong) NSString* user;
@property (strong) NSString* type;
@property (strong) NSNumber* idImage;
@property (strong) NSString* userImageURL;
@property (strong) NSNumber* imageHeight;
    
-(void)initTags:(NSString *)tags;
    
@end
