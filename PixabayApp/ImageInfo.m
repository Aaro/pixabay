//
//  ImageInfo.m
//  PixabayApp
//
//  Created by Надежда Шальнова on 24.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import "ImageInfo.h"

@implementation ImageInfo
    
- (void)initTags:(NSString *)tags {
    tags = [tags stringByReplacingOccurrencesOfString:@" " withString:@","];
    NSMutableArray *arary = [[NSMutableArray alloc] initWithArray:[tags componentsSeparatedByString:@","]];
    [arary removeObject:@""];
    _tags = arary;
}

@end
