//
//  ParseManager.m
//  PixabayApp
//
//  Created by Надежда Шальнова on 24.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import "ParseManager.h"

@implementation ParseManager

+ (NSArray*)parseListRequest:(NSData*) data {
    NSMutableArray * dataArray = [[NSMutableArray alloc]init];
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSDictionary *hits = dictionary[@"hits"];
    for (id hit in  hits) {
        ImageInfo* info = [[ImageInfo alloc] init];
        info.previewHeight = hit[@"previewHeight"];
        info.likes = hit[@"likes"];
        info.favorites = hit[@"favorites"];
        info.user = hit[@"user"];
        info.webformatHeight = hit[@"webformatHeight"];
        info.views = hit[@"views"];
        info.webformatWidth = hit[@"webformatWidth"];
        info.previewWidth = hit[@"previewWidth"];
        info.comments = hit[@"comments"];
        info.downloads = hit[@"downloads"];
        info.pageURL = hit[@"pageURL"];
        info.previewURL = hit[@"previewURL"];
        info.webformatURL = hit[@"webformatURL"];
        info.imageWidth = hit[@"imageWidth"];
        info.user_id = hit[@"user_id"];
        info.user = hit[@"user"];
        info.type = hit[@"type"];
        info.idImage = hit[@"id"];
        info.userImageURL = hit[@"userImageURL"];
        info.imageHeight = hit[@"imageHeight"];
        [info initTags: hit[@"tags"]];
        
        [dataArray addObject:info];
    }
    NSArray *array = [NSArray arrayWithArray:dataArray];
    return array;
}

@end
