//
//  ListImageTableViewCell.m
//  PixabayApp
//
//  Created by Надежда Шальнова on 25.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import "ListImageTableViewCell.h"


@interface ListImageTableViewCell()

@end

@implementation ListImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)buttonClicked:(UIButton*)button {
    NSLog(@"Button %ld clicked.", (long int)[button tag]);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
