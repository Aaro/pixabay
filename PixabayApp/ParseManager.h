//
//  ParseManager.h
//  PixabayApp
//
//  Created by Надежда Шальнова on 24.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageInfo.h"

@interface ParseManager : NSObject

+ (NSArray*)parseListRequest:(NSData*) data;

@end
