//
//  ListImageTableViewCell.h
//  PixabayApp
//
//  Created by Надежда Шальнова on 25.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListImageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *likeLabel;
@property (weak, nonatomic) IBOutlet UILabel *viewLabel;

@property (weak, nonatomic) IBOutlet UILabel *favoriteLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (weak, nonatomic) IBOutlet UICollectionView *tagsCollection;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagsCollectionHeight;
    
@end
