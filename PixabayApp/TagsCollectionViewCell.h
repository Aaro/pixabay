//
//  TagsCollectionViewCell.h
//  PixabayApp
//
//  Created by Надежда Шальнова on 29.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TagsCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end
