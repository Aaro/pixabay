//
//  RequestManager.h
//  PixabayApp
//
//  Created by Надежда Шальнова on 24.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParseManager.h"


@interface RequestManager : NSObject

typedef void(^OnSuccess)(NSArray* params);
typedef void(^OnFailure)(NSError* error);

+ (RequestManager *)sharedInstance;

- (void)sendGetListRequestSuccess:(OnSuccess)success failure:(OnFailure)failure;
- (void)sendGetListRequestWithText:(NSString *)text success:(OnSuccess)success failure:(OnFailure)failure;

@end


@protocol RequestDelegate <NSObject>
- (void) requestUpdateData:(RequestManager *) request;
@end


