//
//  ListViewController.h
//  PixabayApp
//
//  Created by Надежда Шальнова on 24.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@end
