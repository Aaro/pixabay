//
//  DetailsViewController.m
//  PixabayApp
//
//  Created by Надежда Шальнова on 24.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()
@property (strong, nonatomic) ImageInfo* info;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadImageWithUrlString: self.info.webformatURL forImageView:self.imageView];
    [self loadImageWithUrlString: self.info.webformatURL forImageView:self.backgroundImageView];
    [self loadImageWithUrlString: self.info.userImageURL forImageView:self.userImageView];
    self.userLabel.text = self.info.user;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)setImageInfo:(ImageInfo*) info {
    self.info = info;
}


- (void)loadImageWithUrlString:(NSString *)urlString  forImageView:(UIImageView *)imageView {
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: urlString]];
        if ( data == nil ) {
            imageView.image = [UIImage imageNamed:@"fail1"];
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            imageView.image = [UIImage imageWithData: data];
        });
    });
}

@end
