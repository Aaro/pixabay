//
//  ListViewController.m
//  PixabayApp
//
//  Created by Надежда Шальнова on 24.05.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import "ListViewController.h"
#import "RequestManager.h"
#import "ImageInfo.h"
#import "ListImageTableViewCell.h"
#import "DetailsViewController.h"
#import "ErrorManager.h"
#import "TagsCollectionViewCell.h"


@interface ListViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topTableView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray* dataArray;
@property (assign, nonatomic) CGFloat rowHeight;
@property (assign, nonatomic) NSInteger countSection;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (assign, nonatomic) CGFloat heightSearchView;
@end


@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.rowHeight = 85;
    self.heightSearchView = 44;
    self.countSection = 1;
    [self sendRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*----------------------------*/
#pragma mark - actions
/*----------------------------*/
- (IBAction)pressedSearchButtun:(id)sender {
    if (self.searchView.isHidden) {
        [self setView:self.searchView hidden:NO];
        [self moveUpTableView:NO];
    } else {
        [self setView:self.searchView hidden:YES];
        [self moveUpTableView:YES];
    }
}

- (void)presentSearchBar {
    if (self.searchView.isHidden) {
        [self setView:self.searchView hidden:NO];
        [self moveUpTableView:NO];
    }
}

- (void)moveUpTableView:(BOOL)up {
     dispatch_async(dispatch_get_main_queue(), ^{
    if (up) {
        [UIView transitionWithView:self.tableView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void) {
            self.topTableView.constant -= self.heightSearchView;
            } completion:nil];
    } else {
        [UIView transitionWithView:self.tableView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void) {
             self.topTableView.constant += self.heightSearchView;
            } completion:nil];
    } });
}

- (void)setView:(UIView*)view hidden:(BOOL)hidden {
    [UIView transitionWithView:view duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
            [view setHidden:hidden];
        } completion:nil];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.searchTextField resignFirstResponder];
}

- (void)dismissKeyboard {
    [ self.searchTextField resignFirstResponder];
}

- (IBAction)pressedSortButton:(id)sender {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Sort"
                                          message:@"Select the sort type"
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *likesSort = [UIAlertAction actionWithTitle:@"by likes" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              NSArray *sortedArray;
                                                              sortedArray = [self.dataArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                                                                  NSNumber *first = [(ImageInfo*)a likes];
                                                                  NSNumber *second = [(ImageInfo*)b likes];
                                                                  return [first compare:second];
                                                              }];
                                                              self.dataArray = sortedArray;
                                                              [self.tableView reloadData];
                                                          }];
    [alertController addAction:likesSort];

    UIAlertAction *viewsSort = [UIAlertAction actionWithTitle:@"by views" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          NSArray *sortedArray;
                                                          sortedArray = [self.dataArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                                                              NSNumber *first = [(ImageInfo*)a views];
                                                              NSNumber *second = [(ImageInfo*)b views];
                                                              return [first compare:second];
                                                          }];
                                                          self.dataArray = sortedArray;
                                                          [self.tableView reloadData];
                                                      }];
    [alertController addAction:viewsSort];
    
    UIAlertAction *favoritesSort = [UIAlertAction actionWithTitle:@"by favorites" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          NSArray *sortedArray;
                                                          sortedArray = [self.dataArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                                                              NSNumber *first = [(ImageInfo*)a favorites];
                                                              NSNumber *second = [(ImageInfo*)b favorites];
                                                              return [first compare:second];
                                                          }];
                                                          self.dataArray = sortedArray;
                                                          [self.tableView reloadData];
                                                      }];
    [alertController addAction:favoritesSort];
    
    UIAlertAction *cansel = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) { }];
    [alertController addAction:cansel];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)pressedSearchButton:(id)sender {
    [self sendRequestWithText:[self.searchTextField text]];
    [self.searchTextField resignFirstResponder];
}

- (void) addTextInSearchView:(NSString *)text {
    NSString* textFieldText = [self.searchTextField text];
    if  (![textFieldText  isEqual: @""]) {
        self.searchTextField.text = [NSString stringWithFormat:@"%@, %@", textFieldText,  text];
    } else {
        self.searchTextField.text = text;
    }
}


/*----------------------------*/
#pragma mark - tableView
/*----------------------------*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self countSection];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ImageInfo* info = self.dataArray[indexPath.row];
    static NSString *simpleTableIdentifier = @"ListImageTableViewCell";
    ListImageTableViewCell* cell = (ListImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.frame = tableView.bounds;
    [cell layoutIfNeeded];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListImageTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [self loadImageWithUrlString:info.previewURL cell:cell];
    cell.likeLabel.text = [@"likes: " stringByAppendingString:[info.likes stringValue]];
    cell.viewLabel.text = [@"views: " stringByAppendingString:[info.views stringValue]];
    cell.favoriteLabel.text = [@"favorites: " stringByAppendingString:[info.favorites stringValue]];
    cell.tagsCollection.delegate = self;
    cell.tagsCollection.dataSource = self;
    cell.tagsCollection.tag = indexPath.row;
    [cell.tagsCollection registerClass:[TagsCollectionViewCell class] forCellWithReuseIdentifier:@"TagsCollectionViewCell"];
    [cell.tagsCollection reloadData];
    cell.tagsCollectionHeight.constant = cell.tagsCollection.collectionViewLayout.collectionViewContentSize.height;
    UINib *cellNib = [UINib nibWithNibName:@"TagsCollectionViewCell" bundle:nil];
    [cell.tagsCollection registerNib:cellNib forCellWithReuseIdentifier:@"TagsCollectionViewCell"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    ImageInfo* info = self.dataArray[indexPath.row];
    CGFloat height = [info.previewHeight floatValue];
    if (height < [self rowHeight]) {
        return [self rowHeight];
    }
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ImageInfo* info = self.dataArray[indexPath.row];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailsViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"DetailsViewController"];
    [controller setImageInfo:info];
    [self.navigationController pushViewController:controller animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self sendRequestWithText:[textField text]];
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}


/*----------------------------*/
#pragma mark - requests
/*----------------------------*/
-(void)sendRequest {
    dispatch_async(dispatch_get_global_queue(0,0), ^(void){
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinner.center = self.view.center;
        spinner.transform = CGAffineTransformMakeScale(3, 3);
        [spinner setColor:[UIColor whiteColor]];
        [self.view addSubview:spinner];
        [[RequestManager sharedInstance] sendGetListRequestSuccess:^(NSArray * answer) {
            NSArray *sortedArray;
            sortedArray = [answer sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                NSNumber *first = [(ImageInfo*)a likes];
                NSNumber *second = [(ImageInfo*)b likes];
                return [first compare:second];
            }];
            self.dataArray = sortedArray;
            dispatch_async(dispatch_get_main_queue(), ^{
                [spinner stopAnimating];
                [self.tableView reloadData];
            });
        } failure:^(NSError * error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [spinner stopAnimating];
                [ErrorManager showErrorMessange:error.localizedDescription onVC:self];
            });
        }];
    });
}

- (void)loadImageWithUrlString:(NSString *) urlString cell:(ListImageTableViewCell*) cell  {
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: urlString]];
        if ( data == nil ) {
            cell.image.image = [UIImage imageNamed:@"fail1"];
            cell.backgroundImageView.image = [UIImage imageNamed:@"fail1"];
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.image.image = [UIImage imageWithData: data];
            cell.backgroundImageView.image = [UIImage imageWithData: data];
        });
    });
}

- (void)sendRequestWithText:(NSString*) text {
    text = [self prepareTextForRequest:text];
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = self.view.center;
    spinner.transform = CGAffineTransformMakeScale(3, 3);
    [spinner setColor:[UIColor whiteColor]];
    [self.view addSubview:spinner];
    [spinner startAnimating];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [[RequestManager sharedInstance] sendGetListRequestWithText:text  success:^(NSArray * answer) {
            
            NSArray *sortedArray;
            sortedArray = [answer sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                NSNumber *first = [(ImageInfo*)a likes];
                NSNumber *second = [(ImageInfo*)b likes];
                return [first compare:second];
            }];
            self.dataArray = sortedArray;
            dispatch_async(dispatch_get_main_queue(), ^{
                [spinner stopAnimating];
                [self.tableView reloadData];
            });
        } failure:^(NSError * error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [spinner stopAnimating];
                [ErrorManager showErrorMessange:error.localizedDescription onVC:self];
            });
        }];
    });
}

- (NSString*)prepareTextForRequest:(NSString*) text {
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    text = [text stringByReplacingOccurrencesOfString:@"," withString:@""];
    return text;
}


/*----------------------------*/
#pragma mark - collectionView
/*----------------------------*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.dataArray != nil) {
        NSInteger index = collectionView.tag;
        ImageInfo* info = self.dataArray[index];
        NSInteger count = info.tags.count;
        return count;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = cv.tag;
    ImageInfo* info = self.dataArray[index];
    TagsCollectionViewCell *cell = (TagsCollectionViewCell*)[cv dequeueReusableCellWithReuseIdentifier:@"TagsCollectionViewCell" forIndexPath:indexPath];
    cell.textLabel.text = info.tags[indexPath.row];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = collectionView.tag;
    ImageInfo* info = self.dataArray[index];
    CGSize size = [info.tags[indexPath.row] sizeWithAttributes:NULL];
    size.width += 10;
    return size;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = collectionView.tag;
    ImageInfo* info = self.dataArray[index];
    NSString* textTag = info.tags[indexPath.row];
    [self addTextInSearchView: textTag];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentSearchBar];
    });
}

@end
